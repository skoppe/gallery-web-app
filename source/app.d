import vibe.d;
import derelict.freeimage.freeimage;
import std.base64;

auto getBitmapFromBase64(T)(T[] base64)
{
	return getBitmapFromBinary(Base64.decode(cast(char[])base64));
}
auto getBitmapFromBinary(ubyte[] data)
{
	auto fimem = FreeImage_OpenMemory(cast(BYTE*)data.ptr,data.length);
	return FreeImage_LoadFromMemory(FIF_JPEG,fimem);
}
struct ResizeResult
{
	int width;
	int height;
	bool error = false;
}
auto writeThumbnail(I)(I image, int maxSize, string filename)
{
	auto output = filename.dup ~ ".jpeg\0";
	ResizeResult rr;
	rr.width = FreeImage_GetWidth(image);
	rr.height = FreeImage_GetHeight(image);
	if (rr.width <= maxSize && rr.height <= maxSize)
	{
		rr.error = cast(bool)FreeImage_Save(FIF_JPEG,image,cast(char*)output,JPEG_BASELINE | JPEG_OPTIMIZE | JPEG_PROGRESSIVE | 90);
		return rr;
	}

	auto thumb = FreeImage_MakeThumbnail(image,maxSize);
	rr.width = FreeImage_GetWidth(thumb);
	rr.height = FreeImage_GetHeight(thumb);
	rr.error = cast(bool)FreeImage_Save(FIF_JPEG,thumb,cast(char*)output,JPEG_BASELINE | JPEG_OPTIMIZE | JPEG_PROGRESSIVE | 90);
	FreeImage_Unload(thumb);
	return rr;
}

alias Bitmap = FIBITMAP*;

struct Response
{
	bool success;
	string uuid;
}

struct Version
{
	int width;
	int height;
}

struct Meta
{
	string digest;
	string date;
	Version[string] versions;
}

auto extractDate(Image)(Image image)
{
	FITAG* tag;
	auto handle = FreeImage_FindFirstMetadata(FIMD_EXIF_MAIN,image, &tag);
	if (handle)
	{
		do
		{
			auto rawkey = FreeImage_GetTagKey(tag);
			auto type = FreeImage_GetTagType(tag);
			auto value = FreeImage_GetTagValue(tag);
			auto len = strlen(rawkey);
			const char[] key = rawkey[0..len];
			if (key == "DateTime")
			{
				import std.regex;
				auto str = cast(char*)value;
				auto rawdate = str[0..strlen(str)];
				auto regex = ctRegex!(`^([0-9]{4})[^0-9]+([0-9]{2})[^0-9]+([0-9]{2})[^0-9]+([0-9]{2})[^0-9]+([0-9]{2})[^0-9]+([0-9]{2})$`);
				auto m = rawdate.matchFirst(regex);
				if (m.length == 7)
				{
					import std.format;
					auto writer = appender!string();
					writer.formattedWrite("%s-%s-%sT%s:%s:%s",m[1],m[2],m[3],m[4],m[5],m[6]);
					return writer.data;
				}
			}else if (key == "Orientation")
			{
				auto number = *(cast(ushort*)value);
				//writefln("%s",number);
				// handle orientation
			}
		} while (FreeImage_FindNextMetadata(handle,&tag));
		FreeImage_FindCloseMetadata(handle);
	}
	auto now = DateTime();
	return now.toISOExtString();
}

class MetaStore
{
	import std.stdio;
	import std.algorithm;
	import std.conv;
	private Meta[] data;
	private string filename;
	this(string filename)
	{
		import std.file;
		this.filename = filename;
		if (exists(filename))
			data = File(filename).byLine.joiner.text.parseJsonString.deserializeJson!(Meta[]);
	}
	void add(string digest, string date, ResizeResult lg, ResizeResult md, ResizeResult sm)
	{
		data ~= Meta(digest, date, ["lg":Version(lg.width,lg.height),"md":Version(md.width,md.height),"sm":Version(sm.width,sm.height)]);
		store();
	}
	void store()
	{
		import std.stdio;
		auto f = File(filename,"w");
		scope (exit) f.close();
		import vibe.data.json;
		serializeToJson(f.lockingTextWriter(),data);
	}
	@property Meta[] images()
	{
		return data;
	}
}

string createDigest(T)(T data)
{
	import std.digest.sha;
	import std.conv;
	SHA256 digest;
	digest.put(data);
	return to!string(toHexString(digest.finish())[0..64]);
}
template injector(Type)
{
	static Type _instance;
	Type get()
	{
		return _instance;
	}
	void set(Type instance)
	{
		_instance = instance;
	}
}
void upload(HTTPServerRequest req, HTTPServerResponse res, ref Settings settings)
{
	import std.stdio;
	import std.uuid;
	auto data = req.bodyReader.readAll();
	auto digest = data.createDigest();
	import std.file;
	if (exists(settings.storage~"/lg/"~digest~".jpeg"))
	{
		res.writeJsonBody(Response(true,digest));
		return;
	}
	writeln("Receiving image");
	Bitmap bitmap;
	if (req.headers.get("Content-Transfer-Encoding") == "base64")
		bitmap = getBitmapFromBase64(data);
	else
		bitmap = getBitmapFromBinary(data);
	if (bitmap)
	{
		auto lg = writeThumbnail(bitmap,1024,settings.storage~"/lg/"~digest);
		auto md = writeThumbnail(bitmap,386,settings.storage~"/md/"~digest);
		auto sm = writeThumbnail(bitmap,144,settings.storage~"/sm/"~digest);

		string date = extractDate(bitmap);
		injector!MetaStore.get.add(digest,date,lg,md,sm);
		// need to call FreeImage_CloseMemory
		FreeImage_Unload(bitmap);
	}
	res.writeJsonBody(Response(true,digest));
}
void serveApp(HTTPServerRequest req, HTTPServerResponse res)
{
	auto images = injector!(MetaStore).get().images.serializeToJsonString();
	res.headers["Content-Type"] = "text/html";
	res.render!("app.dt", req, images);
}
struct Settings
{
	string storage = "./public/uploads";
	string sslCert;
	string sslKey;
	ushort port = 8080;
	bool validate()
	{
		Path path = Path(storage);
		storage = path.toNativeString();
		return true;
	}
}
auto readSettings()
{
	import std.process;
	string storage = environment.get("PHOTO_STORAGE_FOLDER", "./public/uploads/");
	string sslCert = environment.get("PHOTO_STORAGE_SSLCERT", "");
	string sslKey = environment.get("PHOTO_STORAGE_SSLKEY", "");
	ushort port = environment.get("PHOTO_SERVICE_PORT", "8080").to!ushort;
	auto set = Settings(storage,sslCert,sslKey,port);
	readOption(
		"storage",
		&set.storage,
		"Storage Folder (defaults to \"./public/uploads\")"
	);
	readOption(
		"port",
		&set.port,
		"Listening Port (default to 8080)"
	);
	readOption(
		"chain",
		&set.sslCert,
		"SSL CA Certificate Chain"
	);
	readOption(
		"key",
		&set.sslKey,
		"SSL Private Key"
	);
	return set;
}
bool areValidSettings(ref Settings settings)
{
	if (settings.sslCert == "" && settings.sslKey != "")
	{
		logInfo("Setting a ssl key requires ssl certificate chain");
		return false;
	}
	if (settings.sslCert != "" && settings.sslKey == "")
	{
		logInfo("Setting a ssl certificate chain requires a ssl key");
		return false;
	}
	return settings.validate();
}
auto createServerSettings(Settings settings)
{
	auto ss = new HTTPServerSettings;
	ss.port = settings.port;
	ss.bindAddresses = ["0.0.0.0"];
	ss.useCompressionIfPossible = true;

	if (settings.sslCert != "" && settings.sslKey != "")
	{
		ss.sslContext = createSSLContext(SSLContextKind.server);
		ss.sslContext.useCertificateChainFile(settings.sslCert);
		ss.sslContext.usePrivateKeyFile(settings.sslKey);
	}
	return ss;
}
void forceFolders(Settings settings)
{
	import std.file : mkdirRecurse;
	foreach (string f; ["lg","md","sm"])
		mkdirRecurse(settings.storage~"/"~f);
}
void main()
{
	auto settings = readSettings();
	if (!finalizeCommandLineOptions() || !areValidSettings(settings))
		return;

	import std.stdio : writefln;
	writefln("Using Settings: %s",settings);
	forceFolders(settings);

	DerelictFI.load();
	FreeImage_Initialise();

	injector!MetaStore.set(new MetaStore(settings.storage~"/meta.json"));

	auto router = new URLRouter;
	auto fsettings = new HTTPFileServerSettings;
	fsettings.maxAge = 31536000.seconds;
	void doUpload(HTTPServerRequest req, HTTPServerResponse res)
	{
		upload(req,res,settings);
	}
	router.get("/", &serveApp);
	router.get("/images", serveStaticFile(settings.storage~"/meta.json"));
	router.get("/*", serveStaticFiles(settings.storage, fsettings));
	router.get("/css/*", serveStaticFiles("./public/", fsettings));
	router.post("/uploads", &doUpload);


	auto serverSettings = createServerSettings(settings);
	listenHTTP(serverSettings, router);

	logInfo("Server Started...");
	runEventLoop();

	FreeImage_DeInitialise();
}