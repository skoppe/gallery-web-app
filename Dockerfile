FROM l3iggs/archlinux:latest

MAINTAINER skoppe <bas@21brains.com>

RUN pacman -S --noconfirm freeimage libevent

ENV PHOTO_STORAGE_FOLDER /photos
ENV PHOTO_SERVICE_PORT 80

WORKDIR /app
ADD public/css/bootstrap.css /app/public/css/bootstrap.css
ADD gallery-web-app /app/gallery-web-app

VOLUME ["/photos"]

EXPOSE 80

CMD ./gallery-web-app