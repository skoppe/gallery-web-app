#!/bin/bash
docker run --rm -v $PWD:/app -v /data/.dub:/root/.dub skoppe/ddev /app/build.sh
docker build -t skoppe/photo-app .